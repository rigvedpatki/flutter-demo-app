import 'package:flutter/material.dart';

import './text_output.dart';

class TextControl extends StatefulWidget {
  final String newText;

  TextControl(this.newText);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return TextState();
  }
}

class TextState extends State<TextControl> {
  String textToBeChanged =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Column(
      children: <Widget>[
        Card(
          child: TextOutput(textToBeChanged),
        ),
        RaisedButton(
          onPressed: () {
            setState(() {
              textToBeChanged = widget.newText;
            });
          },
          child: Text('Change Text'),
        )
      ],
    );
  }
}
