import 'package:flutter/material.dart';

class TextOutput extends StatelessWidget {
  final String textToBeDisplayed;

  TextOutput(this.textToBeDisplayed);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(textToBeDisplayed);
  }
}
